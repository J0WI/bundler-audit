package main

import (
	"log"
	"os"

	"github.com/urfave/cli"
	"gitlab.com/gitlab-org/security-products/analyzers/bundler-audit/v2/plugin"
	"gitlab.com/gitlab-org/security-products/analyzers/common/v2/command"
)

func main() {
	app := cli.NewApp()
	app.Name = "analyzer"
	app.Usage = "bundler-audit analyzer for GitLab Dependency Scanning"
	app.Author = "GitLab"

	app.Commands = command.NewCommands(command.Config{
		ArtifactName: command.ArtifactNameDependencyScanning,
		Match:        plugin.Match,
		Analyze:      analyze,
		AnalyzeFlags: analyzeFlags(),
		Convert:      convert,
	})

	if err := app.Run(os.Args); err != nil {
		log.Fatal(err)
	}
}
