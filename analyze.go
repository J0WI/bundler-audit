package main

import (
	"bytes"
	"io"
	"io/ioutil"
	"log"
	"os"
	"os/exec"
	"strings"

	"github.com/urfave/cli"
)

const (
	flagUpdateDisabled    = "update-disabled"
	flagAdvisoryDBURL     = "advisory-db-url"
	flagAdvisoryDBRefName = "advisory-db-ref-name"

	advisoryPath = "/root/.local/share/ruby-advisory-db"

	defaultAdvisoryDBURL = "https://github.com/rubysec/ruby-advisory-db.git"
)

func analyzeFlags() []cli.Flag {
	return []cli.Flag{
		cli.BoolFlag{
			Name:   flagUpdateDisabled,
			Usage:  "Do not update the advisory database before scanning",
			EnvVar: "BUNDLER_AUDIT_UPDATE_DISABLED",
		},
		cli.StringFlag{
			Name:   flagAdvisoryDBURL,
			Usage:  "The url of the bundler-audit advisory db",
			EnvVar: "BUNDLER_AUDIT_ADVISORY_DB_URL",
			Value:  "https://github.com/rubysec/ruby-advisory-db.git",
		},
		cli.StringFlag{
			Name:   flagAdvisoryDBRefName,
			Usage:  "The name of the git ref to use when checking out the advisory db",
			EnvVar: "BUNDLER_AUDIT_ADVISORY_DB_REF_NAME",
			Value:  "master",
		},
	}
}

func analyze(c *cli.Context, path string) (io.ReadCloser, error) {
	// explicitly set $HOME so that bundler-audit gem can find the advisory db when running under docker-in-docker mode
	// (i.e. dependency-scanning orchestrator may reset $HOME)
	os.Setenv("HOME", "/root")

	args := []string{"audit", "check", "--quiet"}
	if !c.Bool(flagUpdateDisabled) {
		if err := updateCustomAdvisoryDB(c); err != nil {
			return nil, err
		}
	}

	cmd := exec.Command("bundle", args...)
	cmd.Dir = path
	cmd.Env = os.Environ()
	cmd.Stderr = os.Stderr

	output, err := cmd.Output()
	if err != nil {
		// bundler-audit exits with code 1 when they are vulnerabilities,
		// so ignore the exit code if the output contains "Vulnerabilities found!".
		if !strings.Contains(string(output), "Vulnerabilities found!") {
			if _, err := os.Stdout.Write(output); err != nil {
				log.Println(err)
			}
			return nil, err
		}
	}

	return ioutil.NopCloser(bytes.NewReader(output)), nil
}

func updateCustomAdvisoryDB(c *cli.Context) error {
	git := func(args ...string) *exec.Cmd {
		args = append([]string{"-C", advisoryPath}, args...)
		cmd := exec.Command("git", args...)
		cmd.Stdout = os.Stdout
		cmd.Stderr = os.Stderr
		return cmd
	}

	argsList := [][]string{
		{"remote", "set-url", "origin", c.String(flagAdvisoryDBURL)},
		{"remote", "update"},
		{"checkout", c.String(flagAdvisoryDBRefName)},
	}

	for _, args := range argsList {
		if err := git(args...).Run(); err != nil {
			return err
		}
	}

	return nil
}
