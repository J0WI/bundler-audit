# bundler-audit analyzer changelog

## v2.5.0
- Add `id` field to vulnerabilities in JSON report (!36)

## v2.4.1
- Fix git error when setting `BUNDLER_AUDIT_ADVISORY_DB_REF_NAME` to a commit hash (!32).

## v2.4.0
- Add support for custom CA certs (!31)

## v2.3.0
- Add `BUNDLER_AUDIT_ADVISORY_DB_URL` and `BUNDLER_AUDIT_ADVISORY_DB_REF_NAME` variables, used to customize the advisory db to be used by the `bundler-audit` gem.

## v2.2.0
- Add `BUNDLER_AUDIT_UPDATE_DISABLED` variable, set this variable true so that bundler-audit does not auto update.

## v2.1.2
- Upgrade common to v.2.1.6

## v2.1.1
- Upgrade common to v.2.1.4, add remediations key and stable sort

## v2.1.0
- Upgrade to bundler-audit 0.6.1, bundler 2.0.1 (Jorn van de beek)

## v2.0.0
- Switch to new report syntax with `version` field

## v1.1.0
- Add dependency (package name and version) to report
- Improve compare key, remove vulnerability name

## v1.0.0
- Initial release
